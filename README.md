# AMIV Error Pages / Maintenance Page

There are some generic error pages, which can be used as custom error pages for traefik.

We minify the pages before we add them to the final docker image. We use `html-minifier`
which is already installed in the devcontainer.

You can manually minify the HTML files with the script `./minify.sh`.
