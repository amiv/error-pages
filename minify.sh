#!/bin/bash

script_path=$(dirname "$0")
output_dir="${script_path}/minified"

mkdir -p $output_dir

for i in `find $script_path -name "*.shtml" -type f -not -path "./minified/*"`; do
    echo "> $i"
    rel_dir=$(dirname "$i")

    mkdir -p "${output_dir}/${rel_dir}"
    html-minifier -c "${script_path}/minify.conf" -o "./minified${i:1}" $i
done
