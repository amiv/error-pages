## First Stage: Minify error pages
FROM node:18 as build

# Install html-minifier package 
RUN npm install html-minifier -g

WORKDIR /build

COPY . .

# Minify all HTML files
RUN ./minify.sh

## Second Stage: Final nginx image 
FROM nginx:1.22-alpine

COPY --from=build /build/minified /var/www
COPY nginx.conf /etc/nginx/conf.d/default.conf

# Copy HTML files
COPY . /var/www/
